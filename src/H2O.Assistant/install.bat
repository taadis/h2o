:: use Topshelf
:: cd /d %~dp0
:: H2O.Assistant install
:: H2O.Assistant start
:: pause

:: use sc.exe
:: step1:
set pwd=%~dp0
set app=H2O.Assistant.exe
set binPath=%pwd%%app%
set serviceName=h2o
cd /d %~dp0
:: step2:
sc.exe create %serviceName% binPath= %binPath% DisplayName= %serviceName% start= auto
:: step3: update service descripiton
sc.exe description %serviceName% "h2o service"
:: step4:
sc.exe query %serviceName%
:: step5:
sc.exe start %serviceName%
:: step6:
pause
