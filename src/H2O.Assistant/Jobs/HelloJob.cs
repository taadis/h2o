﻿using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Threading.Tasks;

namespace H2O.Assistant.Jobs
{
    /// <summary>
    /// 打个招呼作业
    /// </summary>
    public class HelloJob : IJob, IDisposable
    {
        private readonly ILogger<HelloJob> _logger;

        public HelloJob(ILogger<HelloJob> logger)
        {
            _logger = logger;
        }

        public Task Execute(IJobExecutionContext context)
        {
            //return Console.Out.WriteLineAsync(value: $"Hello from {nameof(HelloJob)}");
            //return Task.FromResult(result: true);
            // 使用 NLog.ILogger 输出,
            // 如何用内置的 ILogger 代替?
            // NLog.ILogger logger = NLog.LogManager.GetCurrentClassLogger();
            _logger.LogInformation(message: $"Hello from {nameof(HelloJob)} {DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}");
            return Task.FromResult(result: true);
        }

        public void Dispose()
        {
            _logger.LogInformation("Hello job disposing");
        }
    }
}
