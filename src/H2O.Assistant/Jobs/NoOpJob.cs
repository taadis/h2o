﻿using Quartz;
using System.Threading.Tasks;

namespace H2O.Assistant.Jobs
{
    /// <summary>
    /// 无操作作业
    /// </summary>
    public class NoOpJob : IJob
    {
        public Task Execute(IJobExecutionContext context)
        {
            return Task.FromResult(result: true);
        }
    }
}
