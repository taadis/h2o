﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace H2O.Assistant.HostedServices
{
    /// <summary>
    /// .NET Core 3.x 新建项目时默认带的示例程序, 这里保留, 做个参考.    
    /// </summary>
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation(message: "Worker running at: {time}", args: DateTimeOffset.Now);
                await Task.Delay(millisecondsDelay: 1000, cancellationToken: stoppingToken);
            }
        }
    }
}
