﻿using H2O.Assistant.Jobs;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NLog.Extensions.Logging;
using Quartz;
using Quartz.Job;
using System;

namespace H2O.Assistant
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseWindowsService()
                .UseSystemd()
                .ConfigureServices((hostContext, services) =>
                {
                    if (hostContext.HostingEnvironment.IsDevelopment())
                    {
                        // Development service configuration
                    }
                    else
                    {
                        // Non-development service configuration
                    }

                    // Logging
                    services.AddLogging(options =>
                    {
                        options.AddNLog();
                    });

                    services.AddHttpClient();
                    //services.AddHostedService<LifetimeEventsHostedService>();
                    //services.AddHostedService<TimedHostedService>();
                    //services.AddHostedService<Worker>();

                    /*
                    // Quartz
                    services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
                    services.AddSingleton<IJobFactory, CustomJobFactory>();
                    services.AddHostedService<QuartzHostedService>();
                    // Quartz Jobs
                    services.AddTransient<HelloJob>();
                    services.AddTransient<DingTalkJob>();
                    // 配置其他服务...*/
                    services.AddQuartz(options =>
                    {
                        // 会覆盖 appsetting.json 中的配置
                        options.SchedulerId = "AUTO";

                        // 会覆盖 appsetting.json 中的配置
                        options.SchedulerName = "h2oScheduler";

                        options.UseMicrosoftDependencyInjectionScopedJobFactory(o => o.AllowDefaultConstructor = true);
                        options.UseSimpleTypeLoader();
                        options.UseInMemoryStore();
                        options.UseDefaultThreadPool(o =>
                        {
                            // 最大并发
                            o.MaxConcurrency = 10;
                        });
                        options.UseXmlSchedulingConfiguration(o =>
                        {
                            o.Files = new[] { "~/quartz_jobs.xml" };
                            o.ScanInterval = TimeSpan.FromSeconds(value: 10);
                            o.FailOnFileNotFound = true;
                            o.FailOnSchedulingError = true;
                        });                        
                        // Quartz.Extensions.Hosting hosting for Hosted Services Integration
                        services.AddQuartzHostedService(options =>
                        {
                            // 当关闭时等待服务优雅完成
                            options.WaitForJobsToComplete = true;
                        });
                        services.AddTransient<HelloJob>();
                        services.AddTransient<FileScanJob>();
                    });
                });
    }
}
