# H2O

源自群里一位喝水达人...鄙人有感而发, 遂有此货.

> 当前版本主要体现在钉钉群机器人提醒, 各位看官可加入钉钉群 `23132096` 体验和交流.

## 开发环境

- [Git](https://git-scm.com/)
- [Visual Studio 2019 Community]()
- ...

## 技术栈

- [.NET Core 2.x/3.x](https://dotnet.microsoft.com/)
- [Quartz.NET](https://www.quartz-scheduler.net/)
- [NLog](https://nlog-project.org/)
- ...

## 人体最佳喝水时间表

| 排序 | 时间 | 作用 |
| :---: | :---: | :--- |
| 第一杯水 | 06:30 | 排毒又养颜 |
| 第二杯水 | 08:30 | 体贴又健康 |
| 第三杯水 | 11:00 | 解乏又放松 |
| 第四杯水 | 13:00 | 减负又减肥 |
| 第五杯水 | 15:00 | 提神又醒脑 |
| 第六杯水 | 17:00 | 消化又吸收 |
| 第七杯水 | 19:30 | 滋润又补肤 |
| 第八杯水 | 21:30 | 解毒又排泄 |

## 配置

### 配置钉钉群机器人

通过调用机器人的 webhook 来实现提醒功能

获取 webhook, 见 [自定义机器人](https://ding-doc.dingtalk.com/doc#/serverapi2/qf2nxq)

然后修改 `appsettings.*.json` 以下节点

``` json
{
    "DingTalk": {
        "Enabled": true, // 是否启用
        "WebHook": "https://oapi.dingtalk.com/robot/send?access_token=xxx" // 你的 webhook
    }
}
```

### 部署到 Windows

通过依赖包 `Microsoft.Extensions.Hosting.WindowsServices` 轻松愉快的实现了 Windows Service 的集成.

发布后, 直接 *以管理员身份* 执行 `install.bat` 脚本, 即可自动运行 & 开机启动.

卸载服务请 *以管理员身份* 执行 `uninstall.bat` 脚本, 即可.

放个图:

![](docs/images/windows-service-result.png)

### 部署到 Linux

必备 [运行时环境](https://dotnet.microsoft.com/download/linux-package-manager/rhel/runtime-current), 自行选择并安装

构建完成后的内容打包弄到服务器上

``` sh
# 在发布目录下
# 拷贝本地当前目录下的所有文件到服务器上的指定目录下
scp -r ./* root@[your_ip]:/home/h2o/
```

配置执行权限 `chmod 777 H2O.Assistant`

执行 `./H2O.Assistant` 查看是否运行正常

但更多的时候我们希望部署成守护进程来实现一些自管理

> 这里以我的服务器为例 Debian 9

Debian 9 中内置 *systemd* 那么就用她了

服务配置文件 `h2o.service`

然后执行 `ln -s /home/h2o/h2o.service /etc/systemd/system/h2o.service` 命令, 添加 *systemd* 服务配置的软链接.

可以执行 `systemctl daemon-reload` 重新加载守护进程, 以便刷新配置

可以执行 `systemctl start h2o.service` 命令, 马上启动

可以执行 `systemctl enable h2o.service` 命令, 添加开机启动

可以执行 `systemctl stop h2o.service` 命令, 停止服务

可以执行 `systemctl disable h2o.service` 命令, 卸载服务, 会自动删除软链接

可以执行 `systemctl status h2o.service` 命令, 查看服务状态

附张状态图

![](docs/images/systemd-status-result.png)

## 效果图

### 电脑

![](docs/images/pc-result.png)

### 爪机

![](docs/images/iphone-result.png)

## License

[MIT](LICENSE)
